export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH=$HOME/bin/statusbar:$PATH
export PATH=$HOME/go/bin:$PATH
export PATH=$HOME/.local/bin:$PATH
export PATH=$HOME/bin:$PATH
export PATH="$HOME/.symfony/bin:$PATH"


export PROJECT="canon"
export MAGE_ROOT="/home/user/work/projects/$PROJECT"


export MANPATH="/usr/local/man:$MANPATH"
export LANG=ru_RU.UTF-8
export EDITOR="vim"
export VISUAL="vim"
export TERMINAL="terminator"
export BROWSER="google-chrome-stable"
export READER="zathura"
export PAGER='less -r'
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export ARCHFLAGS="-arch x86_64"

