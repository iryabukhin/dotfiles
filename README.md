# Dotfiles
The whole thing is designed to work with [GNU Stow](https://www.gnu.org/software/stow/)

### Usage
While in the root directory run `stow -vt ~ <directory_name>`, where `<directory_name>` is the directory containing dotfiles
